var mongoose = require('mongoose');

// Event Schema
var EventSchema = mongoose.Schema({
	name: {
		type: String,
		index:true
	},
	date: {
		type: String
	},
});

var Event = module.exports = mongoose.model('Event', EventSchema);

module.exports.createEvent = function(newEvent, callback){
  newEvent.save(callback);
}

module.exports.getEventByName = function(name, callback){
	var Equery = {name: name};
	Event.findOne(Equery, callback);
}

module.exports.getEventById = function(id, callback){
	Event.findById(id, callback);
}
