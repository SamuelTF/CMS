var express = require('express');
var router = express.Router();

var Event = require('../models/events');

 // Register
router.get('/addevent', function(req, res){
	res.render('register');
});


// Add Event
router.post('/addevent', function(req, res){
	var name = req.body.name;
	var date = req.body.date;

	// Validation
	req.checkBody('name', 'Name is required').notEmpty();
	req.checkBody('date', 'Date is required').notEmpty();

	var errors = req.validationErrors();

	if(errors){
		res.render('register',{
			errors:errors
		});
	} else {
		var newEvent = new Event({
			name: name,
			date: date
		});

		Event.createEvent(newEvent, function(err, event){
			if(err) throw err;
			console.log(event);
		});

		req.flash('success_msg', 'You added a event');

		res.redirect('/events');
	}
});

module.exports = router;
