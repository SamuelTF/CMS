var express = require('express');
var mongoose = require('mongoose');
var mongodb = require('mongodb');
var router = express.Router();

var Event = require('../models/events');

// Get Homepage
router.get('/', function(req, res, next){
	Event.find({}, (err, data) => {
		res.render('index', {events:data});
	})
});
// Get Aboutpage
router.get('/about', function(req, res){
	res.render('about');
});

// Get Eventpage
router.get('/events', function(req, res, next){
	Event.find({}, (err, data) => {
		res.render('events', {events:data});
	})
});


module.exports = router;
